package drm

//go:generate go run gen_fourcc.go

import (
	"fmt"
	"strings"
)

type Format uint32

func (f Format) Name() string {
	if name := f.str(); name != "" {
		return name
	}
	return "unknown"
}

func (f Format) String() string {
	return fmt.Sprintf("%s (0x%X)", f.Name(), uint32(f))
}

type ModifierVendor uint8

func (vendor ModifierVendor) String() string {
	if name := vendor.str(); name != "" {
		return name
	}
	return "unknown"
}

type Modifier uint64

func (mod Modifier) Vendor() ModifierVendor {
	return ModifierVendor(mod >> 56)
}

func (mod Modifier) Name() string {
	var name string
	switch mod.Vendor() {
	case ModifierVendorNVIDIA:
		name = modifierNameNVIDIA(uint64(mod))
	case ModifierVendorARM:
		name = modifierNameARM(uint64(mod))
	case ModifierVendorAMLOGIC:
		name = modifierNameAmlogic(uint64(mod))
	case ModifierVendorAMD:
		name = modifierNameAMD(uint64(mod))
	case ModifierVendorVIVANTE:
		name = modifierNameVivante(uint64(mod))
	}
	if name != "" {
		return name
	}

	if name := mod.str(); name != "" {
		return name
	}
	if vendor := mod.Vendor().str(); vendor != "" {
		return fmt.Sprintf("%s(unknown)", strings.ToUpper(vendor))
	}
	return "unknown"
}

func (mod Modifier) String() string {
	return fmt.Sprintf("%s (0x%X)", mod.Name(), uint64(mod))
}

func modifierNameNVIDIA(mod uint64) string {
	if mod&0x10 == 0 {
		return ""
	}

	h := mod & 0xF
	k := (mod >> 12) & 0xFF
	g := (mod >> 20) & 0x3
	s := (mod >> 22) & 0x1
	c := (mod >> 23) & 0x7

	return fmt.Sprintf("NVIDIA_BLOCK_LINEAR_2D(h=%v, k=%v, g=%v, s=%v, c=%v)", h, k, g, s, c)
}

func modifierNameARM(mod uint64) string {
	typ := (mod >> 52) & 0xF
	value := mod & 0x000FFFFFFFFFFFFF

	switch typ {
	case c_DRM_FORMAT_MOD_ARM_TYPE_AFBC:
		blockSize := value & c_AFBC_FORMAT_MOD_BLOCK_SIZE_MASK

		props := []string{
			fmt.Sprintf("BLOCK_SIZE = %v", armAFBCBlockSizeStr(blockSize)),
		}

		if value&c_AFBC_FORMAT_MOD_YTR != 0 {
			props = append(props, "YTR")
		}
		if value&c_AFBC_FORMAT_MOD_SPLIT != 0 {
			props = append(props, "SPLIT")
		}
		if value&c_AFBC_FORMAT_MOD_SPARSE != 0 {
			props = append(props, "SPARSE")
		}
		if value&c_AFBC_FORMAT_MOD_CBR != 0 {
			props = append(props, "CBR")
		}
		if value&c_AFBC_FORMAT_MOD_TILED != 0 {
			props = append(props, "TILED")
		}
		if value&c_AFBC_FORMAT_MOD_SC != 0 {
			props = append(props, "SC")
		}
		if value&c_AFBC_FORMAT_MOD_DB != 0 {
			props = append(props, "DB")
		}
		if value&c_AFBC_FORMAT_MOD_BCH != 0 {
			props = append(props, "BCH")
		}
		if value&c_AFBC_FORMAT_MOD_USM != 0 {
			props = append(props, "USM")
		}

		return fmt.Sprintf("ARM_AFBC(%v)", strings.Join(props, ", "))
	case c_DRM_FORMAT_MOD_ARM_TYPE_AFRC:
		var props []string

		if s := armAFRCCUSize(value & c_AFRC_FORMAT_MOD_CU_SIZE_MASK); s != "" {
			props = append(props, "CU_SIZE_P0 = "+s)
		}
		if s := armAFRCCUSize((value >> 4) & c_AFRC_FORMAT_MOD_CU_SIZE_MASK); s != "" {
			props = append(props, "CU_SIZE_P12 = "+s)
		}

		if value&c_AFRC_FORMAT_MOD_LAYOUT_SCAN != 0 {
			props = append(props, "LAYOUT = SCAN")
		} else {
			props = append(props, "LAYOUT = ROT")
		}

		return fmt.Sprintf("ARM_AFRC(%v)", strings.Join(props, ", "))
	case c_DRM_FORMAT_MOD_ARM_TYPE_MISC:
		// Simple modifiers handled by the fallback path
		return ""
	default:
		return ""
	}
}

func armAFBCBlockSizeStr(blockSize uint64) string {
	switch blockSize {
	case c_AFBC_FORMAT_MOD_BLOCK_SIZE_16x16:
		return "16x16"
	case c_AFBC_FORMAT_MOD_BLOCK_SIZE_32x8:
		return "32x8"
	case c_AFBC_FORMAT_MOD_BLOCK_SIZE_64x4:
		return "64x4"
	case c_AFBC_FORMAT_MOD_BLOCK_SIZE_32x8_64x4:
		return "32x8_64x4"
	default:
		return "unknown"
	}
}

func armAFRCCUSize(cuSize uint64) string {
	switch cuSize {
	case 0:
		return ""
	case c_AFRC_FORMAT_MOD_CU_SIZE_16:
		return "16"
	case c_AFRC_FORMAT_MOD_CU_SIZE_24:
		return "24"
	case c_AFRC_FORMAT_MOD_CU_SIZE_32:
		return "32"
	default:
		return "unknown"
	}
}

func modifierNameAmlogic(mod uint64) string {
	layout := mod & 0xFF
	options := (mod >> 8) & 0xFF

	return fmt.Sprintf("AMLOGIC_FBC(layout = %v, options = %v)", amlogicFBCLayoutStr(layout), amlogicFBCOptionsStr(options))
}

func amlogicFBCLayoutStr(layout uint64) string {
	switch layout {
	case c_AMLOGIC_FBC_LAYOUT_BASIC:
		return "BASIC"
	case c_AMLOGIC_FBC_LAYOUT_SCATTER:
		return "SCATTER"
	default:
		return "unknown"
	}
}

func amlogicFBCOptionsStr(options uint64) string {
	if options == 0 {
		return "0"
	}
	if options == c_AMLOGIC_FBC_OPTION_MEM_SAVING {
		return "MEM_SAVING"
	}
	return "unknown"
}

func modifierNameAMD(mod uint64) string {
	tileVersion := (mod >> c_AMD_FMT_MOD_TILE_VERSION_SHIFT) & c_AMD_FMT_MOD_TILE_VERSION_MASK
	tile := (mod >> c_AMD_FMT_MOD_TILE_SHIFT) & c_AMD_FMT_MOD_TILE_MASK
	dcc := (mod >> c_AMD_FMT_MOD_DCC_SHIFT) & c_AMD_FMT_MOD_DCC_MASK
	dccRetile := (mod >> c_AMD_FMT_MOD_DCC_RETILE_SHIFT) & c_AMD_FMT_MOD_DCC_RETILE_MASK
	dccPipeAlign := (mod >> c_AMD_FMT_MOD_DCC_PIPE_ALIGN_SHIFT) & c_AMD_FMT_MOD_DCC_PIPE_ALIGN_MASK

	s := fmt.Sprintf("AMD(TILE_VERSION = %v, TILE = %v", amdTileVersionStr(tileVersion), amdTileStr(tile, tileVersion))

	if dcc != 0 {
		dccIndependant64B := (mod >> c_AMD_FMT_MOD_DCC_INDEPENDENT_64B_SHIFT) & c_AMD_FMT_MOD_DCC_INDEPENDENT_64B_MASK
		dccIndependant128B := (mod >> c_AMD_FMT_MOD_DCC_INDEPENDENT_128B_SHIFT) & c_AMD_FMT_MOD_DCC_INDEPENDENT_128B_MASK
		dccMaxCompressedBlock := (mod >> c_AMD_FMT_MOD_DCC_MAX_COMPRESSED_BLOCK_SHIFT) & c_AMD_FMT_MOD_DCC_MAX_COMPRESSED_BLOCK_MASK
		dccConstantEncode := (mod >> c_AMD_FMT_MOD_DCC_CONSTANT_ENCODE_SHIFT) & c_AMD_FMT_MOD_DCC_CONSTANT_ENCODE_MASK

		s += ", DCC"
		if dccRetile != 0 {
			s += ", DCC_RETILE"
		}
		if dccRetile == 0 && dccPipeAlign != 0 {
			s += ", DCC_PIPE_ALIGN"
		}
		if dccIndependant64B != 0 {
			s += ", DCC_INDEPENDENT_64B"
		}
		if dccIndependant128B != 0 {
			s += ", DCC_INDEPENDENT_128B"
		}
		s += ", DCC_MAX_COMPRESSED_BLOCK = " + amdDCCBlockSizeStr(dccMaxCompressedBlock)
		if dccConstantEncode != 0 {
			s += ", DCC_CONSTANT_ENCODE"
		}
	}

	if tileVersion >= c_AMD_FMT_MOD_TILE_VER_GFX9 && amdGFX9TileIsX_T(tile) {
		pipeXORBits := (mod >> c_AMD_FMT_MOD_PIPE_XOR_BITS_SHIFT) & c_AMD_FMT_MOD_PIPE_XOR_BITS_MASK
		s += fmt.Sprintf(", PIPE_XOR_BITS = %v", pipeXORBits)
		if tileVersion == c_AMD_FMT_MOD_TILE_VER_GFX9 {
			bankXORBits := (mod >> c_AMD_FMT_MOD_BANK_XOR_BITS_SHIFT) & c_AMD_FMT_MOD_BANK_XOR_BITS_MASK
			s += fmt.Sprintf(", BANK_XOR_BITS = %v", bankXORBits)
		}
		if tileVersion == c_AMD_FMT_MOD_TILE_VER_GFX10_RBPLUS {
			packers := (mod >> c_AMD_FMT_MOD_PACKERS_SHIFT) & c_AMD_FMT_MOD_PACKERS_MASK
			s += fmt.Sprintf(", PACKERS = %v", packers)
		}
		if tileVersion == c_AMD_FMT_MOD_TILE_VER_GFX9 && dcc != 0 {
			rb := (mod >> c_AMD_FMT_MOD_RB_SHIFT) & c_AMD_FMT_MOD_RB_MASK
			s += fmt.Sprintf(", RB = %v", rb)
		}
		if tileVersion == c_AMD_FMT_MOD_TILE_VER_GFX9 && dcc != 0 && (dccRetile != 0 || dccPipeAlign != 0) {
			pipe := (mod >> c_AMD_FMT_MOD_PIPE_SHIFT) & c_AMD_FMT_MOD_PIPE_MASK
			s += fmt.Sprintf(", PIPE = %v", pipe)
		}
	}

	return s + ")"
}

func amdTileVersionStr(tileVersion uint64) string {
	switch tileVersion {
	case c_AMD_FMT_MOD_TILE_VER_GFX9:
		return "GFX9"
	case c_AMD_FMT_MOD_TILE_VER_GFX10:
		return "GFX10"
	case c_AMD_FMT_MOD_TILE_VER_GFX10_RBPLUS:
		return "GFX10_RBPLUS"
	case c_AMD_FMT_MOD_TILE_VER_GFX11:
		return "GFX11"
	}
	return "unknown"
}

func amdTileStr(tile, tileVersion uint64) string {
	switch tileVersion {
	case c_AMD_FMT_MOD_TILE_VER_GFX9, c_AMD_FMT_MOD_TILE_VER_GFX10, c_AMD_FMT_MOD_TILE_VER_GFX10_RBPLUS, c_AMD_FMT_MOD_TILE_VER_GFX11:
		switch tile {
		case c_AMD_FMT_MOD_TILE_GFX9_64K_S:
			return "GFX9_64K_S"
		case c_AMD_FMT_MOD_TILE_GFX9_64K_D:
			return "GFX9_64K_D"
		case c_AMD_FMT_MOD_TILE_GFX9_64K_S_X:
			return "GFX9_64K_S_X"
		case c_AMD_FMT_MOD_TILE_GFX9_64K_D_X:
			return "GFX9_64K_D_X"
		case c_AMD_FMT_MOD_TILE_GFX9_64K_R_X:
			return "GFX9_64K_R_X"
		case c_AMD_FMT_MOD_TILE_GFX11_256K_R_X:
			return "GFX11_256K_R_X"
		}
	}
	return "unknown"
}

func amdDCCBlockSizeStr(size uint64) string {
	switch size {
	case c_AMD_FMT_MOD_DCC_BLOCK_64B:
		return "64B"
	case c_AMD_FMT_MOD_DCC_BLOCK_128B:
		return "128B"
	case c_AMD_FMT_MOD_DCC_BLOCK_256B:
		return "256B"
	}
	return "unknown"
}

func amdGFX9TileIsX_T(tile uint64) bool {
	switch tile {
	case c_AMD_FMT_MOD_TILE_GFX9_64K_S_X, c_AMD_FMT_MOD_TILE_GFX9_64K_D_X, c_AMD_FMT_MOD_TILE_GFX9_64K_R_X:
		return true
	default:
		return false
	}
}

func modifierNameVivante(mod uint64) string {
	ts := mod & c_VIVANTE_MOD_TS_MASK
	comp := mod & c_VIVANTE_MOD_COMP_MASK
	tiling := mod & ^uint64(c_VIVANTE_MOD_EXT_MASK)

	s := fmt.Sprintf("VIVANTE(tiling = %v", vivanteTilingStr(tiling))
	if ts != 0 {
		s += ", ts = " + vivanteTileStatusStr(ts)
	}
	if comp != 0 {
		s += ", comp = " + vivanteCompressionStr(comp)
	}
	s += ")"
	return s
}

func vivanteTilingStr(tiling uint64) string {
	switch Modifier(tiling) {
	case 0:
		return "LINEAR"
	case ModifierVIVANTE_TILED:
		return "TILED"
	case ModifierVIVANTE_SUPER_TILED:
		return "SUPER_TILED"
	case ModifierVIVANTE_SPLIT_TILED:
		return "SPLIT_TILED"
	case ModifierVIVANTE_SPLIT_SUPER_TILED:
		return "SPLIT_SUPER_TILED"
	}
	return "unknown"
}

func vivanteTileStatusStr(ts uint64) string {
	switch ts {
	case c_VIVANTE_MOD_TS_64_4:
		return "64_4"
	case c_VIVANTE_MOD_TS_64_2:
		return "64_2"
	case c_VIVANTE_MOD_TS_128_4:
		return "128_4"
	case c_VIVANTE_MOD_TS_256_4:
		return "256_4"
	}
	return "unknown"
}

func vivanteCompressionStr(comp uint64) string {
	switch comp {
	case c_VIVANTE_MOD_COMP_DEC400:
		return "DEC400"
	}
	return "unknown"
}
