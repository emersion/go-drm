module gitlab.freedesktop.org/emersion/go-drm

go 1.19

require (
	github.com/dave/jennifer v1.7.1
	github.com/rjeczalik/pkgconfig v0.0.0-20190903131546-94d388dab445
	modernc.org/cc/v4 v4.23.1
)

require (
	github.com/remyoudompheng/bigfft v0.0.0-20230129092748-24d4a6f8daec // indirect
	modernc.org/mathutil v1.6.0 // indirect
	modernc.org/opt v0.1.3 // indirect
	modernc.org/sortutil v1.2.0 // indirect
	modernc.org/strutil v1.2.0 // indirect
	modernc.org/token v1.1.0 // indirect
)
